#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import os
import secrets
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "core.settings")

import django
from django.core.exceptions import ObjectDoesNotExist

django.setup()

from core.data.models import *
from core.data.settings import DEF_LANG, MIN_TO_MODIFY, State


def _list_tables():
    from django.apps import apps

    app_models = apps.get_app_config('core').get_models()
    for mod in app_models:
        print(mod._meta.db_table)
        for field in mod._meta.get_fields(include_parents=True, include_hidden=False):
            print('\t', field)


def _create_token(name, start_from=0):
    token = secrets.token_hex(4)

    default_language = Language.objects.get(code=DEF_LANG)

    Judge.objects.create(id=start_from, token=token, full_name=name, lang=default_language)
    return token


def _drop_users_gte_x(x):
    try:
        Judge.objects.filter(id__gte=x).delete()
    except ObjectDoesNotExist:
        pass


def _drop_user_by_tel_id(tel_id):
    try:
        Judge.objects.filter(tel_id=tel_id).delete()
    except ObjectDoesNotExist:
        pass


def _disunite_by_tel_id(tel_id):
    try:
        judge = Judge.objects.get(tel_id=tel_id)
        judge.tel_id = None
        judge.save()
    except ObjectDoesNotExist:
        pass


def get_judge(tel_id):
    try:
        return Judge.objects.get(tel_id=tel_id)
    except ObjectDoesNotExist:
        return False


def get_state(tel_id):
    judge = get_judge(tel_id=tel_id)
    return judge.state


def set_state(tel_id, new_state):
    judge = get_judge(tel_id=tel_id)
    if judge:
        judge.state = new_state
        judge.save()
        return True


def get_lang_obj(lang_code):
    return Labels.objects.language(lang_code).first()


def get_lang_obj_by_id(lang_id):
    return Language.objects.get(id=lang_id)


def fetch_all_langs():
    return Language.objects.all()


def get_db_def_lang():
    return Language.objects.filter(default=True).first()


def confirm_token(user_id, token):
    try:
        judge = Judge.objects.get(token=token)
        judge.rate_scale = 5
        db_def_lang = get_db_def_lang()
        if db_def_lang:
            judge.lang = db_def_lang
        else:
            judge.lang = Language.objects.get(code=DEF_LANG)
        judge.tel_id = user_id
        judge.save()
        return True
    except ObjectDoesNotExist:
        return False


def get_last_transaction(user_id):
    return Transactions.objects.filter(judge__tel_id=user_id).order_by('-timestamp').first()


def get_current_judge_state(user_id):
    try:
        return Judge.objects.get(tel_id=user_id).state
    except ObjectDoesNotExist:
        return State.NULL.value


def get_project_by_id(project_id):
    return Project.objects.get(id=project_id)


def get_round_by_id(round_id):
    return Round.objects.get(id=round_id)


def fetch_teams_for_judge(judge_id, round_obj=None):
    judge = get_judge(judge_id)
    if not round_obj:
        cur_round = fetch_current_round()
    else:
        cur_round = round_obj
    if judge and cur_round:
        workloads = Assignment.objects.filter(judge=judge,
                                              round=cur_round).order_by("project__id")
        return [(wrkld.project.name, wrkld.project.id,
                 workloads.filter(confirmed=False, project=wrkld.project).count())
                for wrkld in workloads]


def fetch_projects_data_by_ids(team_ids, user_id):
    teams = fetch_teams_for_judge(user_id)
    return list(filter(lambda x: str(x[1]) in team_ids, teams))


def fetch_current_round(lang=DEF_LANG):
    today = datetime.datetime.today()
    cur_round = Round.objects.filter(start_timestamp__lte=today, end_timestamp__gte=today).first()
    if cur_round:
        cur_round.set_current_language(lang)
        return cur_round
    return False


def fetch_rounds(judge_id):
    judge = get_judge(judge_id)
    assignments = Assignment.objects.filter(judge__tel_id=judge_id)
    rounds = set()
    for assignment in assignments:
        assignment.round.set_current_language(judge.lang.code)
        rounds.add(assignment.round)
    return rounds


def fetch_project_card_data(project_id, lang=DEF_LANG):
    project_data = {
        "project_id": project_id,
        "project_name": None,
        "project_region": None,
        "project_title": None,
        "competition_id": None,
        "competition_name": None,
        "participants_names": []
    }
    try:
        project = Project.objects.get(id=project_id)
        project.competition.set_current_language(lang)

        team = Participant.objects.filter(project__id=project.id)

        project_data["project_name"] = project.name.replace('_', ' ')
        project_data["project_region"] = project.region
        project_data["project_title"] = project.title.replace('_', ' ')
        project_data["competition_id"] = project.competition.id
        project_data["competition_name"] = project.competition.title.replace('_', ' ')
        project_data["participants_names"] = ', '.join([x.name for x in team])
        return project_data

    except ObjectDoesNotExist:
        return False


def project_eval_status(user_id, project_id):
    current_round = fetch_current_round()
    if not current_round:
        return 2

    workload = Assignment.objects.filter(
        judge__tel_id=user_id,
        round__id=current_round.id,
        project__id=project_id).first()

    fits_time = False
    if workload.last_modified_ts and workload.confirmed:
        now = datetime.datetime.today()
        min_to_modify = datetime.timedelta(minutes=MIN_TO_MODIFY)
        fits_time = bool((workload.last_modified_ts + min_to_modify) > now)

    if not (workload.confirmed or workload.modified):
        return 0
    elif (workload.confirmed and fits_time) and not workload.modified:
        return 1
    else:
        return 2


def project_was_evaluated(judge_id, project_id, round_id):
    workload = Assignment.objects.get(
        judge__tel_id=judge_id,
        round__id=round_id,
        project__id=project_id)
    return workload.confirmed


def get_judge_media(judge_id, project_id, round_id):
    media_files = Media.objects.filter(
        judge__tel_id=judge_id,
        round__id=round_id,
        project__id=project_id)
    return media_files


def create_media(judge, file_id, f_type=None):
    new_media = Media.objects.create(
        judge=judge, project=judge.project,
        round=judge.current_round, media_id=file_id,
        media_type=f_type
    )
    new_media.save()


def get_assignment(judge, project):
    if not judge.current_round:
        current_round = fetch_current_round(judge.lang.code)
    else:
        current_round = judge.current_round

    return Assignment.objects.get(
        judge=judge,
        round=current_round,
        project=project
    )


def fix_rate_scale(judge, project):
    workload = get_assignment(judge, project)
    if workload and not workload.rate_scale:
        workload.rate_scale = judge.rate_scale
        workload.save()


def activate_evaluation_mode(judge_id, project_id, modif=False):
    judge = get_judge(judge_id)
    project = get_project_by_id(project_id)

    if not modif:
        fix_rate_scale(judge, project)

    judge.state = State.EVALUATION.value
    judge.project = project
    judge.criterion = fetch_criterions(judge).first()
    judge.save()


def set_assignment_modified(judge, project_id):
    project = get_project_by_id(project_id)
    assignment = get_assignment(judge, project)
    assignment.modified = True
    assignment.save()


def fetch_criterions(judge=None):
    if judge:
        lang_code = judge.lang.code
    else:
        lang_code = DEF_LANG
    return Criterion.objects.language(lang_code).all().order_by('criterion_group__order', 'order')


def fetch_mark_criterions():
    """
    Get only criterions that aren't read_only
    :return: list of Criterions
    """
    return Criterion.objects.filter(read_only=False).order_by('order')


def update_last_criterion(judge_id, new_criterion):
    judge = get_judge(judge_id)
    criterions = fetch_criterions(judge)
    if new_criterion > len(criterions):
        judge.criterion = criterions.first()
    else:
        judge.criterion = criterions[new_criterion]
        judge.save()


def set_mark(judge, project_id, score):
    project = get_project_by_id(project_id)
    assignment = get_assignment(judge, project)

    mark = fetch_mark(assignment, judge.criterion)
    if mark:
        mark.mark = score
        mark.save()
    else:
        new_mark = Mark(assignment=assignment, criterion=judge.criterion, mark=score)
        new_mark.save()


def all_marks_set(judge, project):
    assignment = get_assignment(judge, project)
    marks = Mark.objects.filter(assignment=assignment, criterion__read_only=False)
    criterions_count = fetch_mark_criterions().count()

    if len(marks) >= criterions_count:
        return True
    else:
        return False


def confirm_finish(judge, project):
    assignment = get_assignment(judge, project)
    assignment.confirmed = True
    assignment.last_modified_ts = datetime.datetime.now()
    assignment.save()


def fetch_first_unset_criteria(judge, project):
    assignment = get_assignment(judge, project)
    marks = Mark.objects.filter(
        assignment=assignment
    )
    marks_ids = [m.criterion.id for m in marks]
    all_criterions = fetch_criterions(judge)
    criterions = list(filter(lambda x: x.id not in marks_ids and not x.read_only, all_criterions))
    if len(criterions) == 0:
        return all_criterions.first()
    return criterions[0]


def get_mark(judge, project_id=None, criterion_num=None, archive=False):
    if project_id:
        project = get_project_by_id(project_id)
    else:
        project = judge.project

    criterions = fetch_criterions(judge)
    criterion = criterions[criterion_num]

    assignment = get_assignment(judge, project)
    if archive and assignment.rate_scale == 0:
        mark = fetch_mark(assignment, criterion)
    else:
        mark = scale_preset_mark(criterion, assignment, assignment.rate_scale)
    return mark


def fetch_mark(assignment, criterion):
    try:
        return Mark.objects.get(
            assignment=assignment,
            criterion=criterion
        )
    except ObjectDoesNotExist:
        return None


def scale_preset_mark(criterion, assignment, rate_scale):
    mark = fetch_mark(assignment, criterion)
    if not mark:
        return None

    if criterion.read_only and mark.mark != 0 and assignment.rate_scale != 0:
        return rate_scale / (assignment.rate_scale / mark.mark)
    elif criterion.read_only:
        return None
    else:
        return mark.mark


def fetch_all_languages():
    pass


if __name__ == '__main__':
    jd = get_judge(89689285)
    jd.rate_scale = 5
    pr = get_project_by_id(23)
    print(jd.current_round)
    fix_rate_scale(jd, pr)
