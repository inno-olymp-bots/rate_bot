#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

from telebot import types

from core import database as db

B_SEARCH = "\U0001F50D"
B_REFRESH = "\U0001F504"
B_HISTORY = "\U0001F5C4"
B_SETTINGS = "\U00002699"
B_SEND_AUDIO = "\U0001F3A4"
B_SEND_VIDEO = "\U0001F3A5"
B_SEND_PHOTO = "\U0001F4F7"
B_CANCEL = "\U0000274C"
B_CONFIRM = "\U00002705 {}"
B_DECLINE = "\U0000274E {}"
B_PREW = "\U000025C0"
B_NEXT = "\U000025B6"
B_BACK = "\U0001F519"

MAIN_MENU_MARKUP = types.ReplyKeyboardMarkup(row_width=4, resize_keyboard=True)
MAIN_MENU_MARKUP.add(types.KeyboardButton(text=B_SEARCH),
                     types.KeyboardButton(text=B_REFRESH),
                     types.KeyboardButton(text=B_HISTORY),
                     types.KeyboardButton(text=B_SETTINGS))

SCALE_MARKUP = types.InlineKeyboardMarkup(row_width=2)
SCALE_MARKUP.add(types.InlineKeyboardButton(text="0..2",
                                            callback_data="/c_ssc -v:2"),
                 types.InlineKeyboardButton(text="0..5",
                                            callback_data="/c_ssc -v:5"),
                 types.InlineKeyboardButton(text=B_PREW,
                                            callback_data="/c_sm"))


def get_detailed_team_select_keyboard(teams_data, archive=False, round_id=None):
    cmd_project_card = "/c_spc -t:{}"
    cmd_select_team_range = "/c_str -t:{}"
    if archive:
        cmd_project_card = "/c_sac -t:{} " + "-r:{}".format(round_id)
        cmd_select_team_range = "/c_str -t:{} " + "-r:{}".format(round_id)
    small = False
    description = ""
    teams_markup = types.InlineKeyboardMarkup(row_width=3)
    ln_teams = len(teams_data)
    if ln_teams <= 9:
        teams_markup = types.InlineKeyboardMarkup(row_width=3)
        buttons = [
            types.InlineKeyboardButton(text=team[0] if not team[2] else team[0] + "*",
                                       callback_data=cmd_project_card.format(team[1]))
            for team in teams_data
        ]
        teams_markup.add(*buttons)
        small = True
    elif ln_teams <= 72:
        teams_data = sorted(teams_data, key=lambda x: x[0])
        buttons = []
        k = ln_teams // 9 + (0 if ln_teams % 9 == 0 else 1)
        teams_ids = [str(team[1]) for team in teams_data]
        teams_names = [str(team[0]) for team in teams_data]
        for i in range(1, k + 1):
            teams_select_command = ";".join(teams_ids[((i - 1) * 9):i * 9])
            teams_butt_description = ", ".join(teams_names[((i - 1) * 9):i * 9])
            call_cmd = cmd_select_team_range.format(teams_select_command)
            buttons.append(
                types.InlineKeyboardButton(text=str(i),
                                           callback_data=call_cmd)
            )
            description += "*{}*. {}\n".format(i, teams_butt_description)
        teams_markup.add(*buttons)
    return {"small": small,
            "markup": teams_markup,
            "description": description}


def get_project_card_inline_keyboard(team_id, text_marks, archive=False, round_id=None):
    team_card_markup = types.InlineKeyboardMarkup(row_width=3)
    team_card_markup.add(
        types.InlineKeyboardButton(text=B_SEND_AUDIO,
                                   callback_data="/c_gv -t:{} -r:{}".format(team_id, round_id)),
        types.InlineKeyboardButton(text=B_SEND_PHOTO,
                                   callback_data="/c_gp -t:{} -r:{}".format(team_id, round_id)),
        types.InlineKeyboardButton(text=B_SEND_VIDEO,
                                   callback_data="/c_gvi -t:{} -r:{}".format(team_id, round_id)))
    if archive:
        if round_id:
            team_card_markup.add(types.InlineKeyboardButton(
                text=text_marks,
                callback_data="/c_sa -t:{} -r:{}".format(team_id, round_id))
            )
        else:
            team_card_markup.add(types.InlineKeyboardButton(
                text=text_marks,
                callback_data="/c_sa -t:{} -r:{}".format(team_id, round_id))
            )
    else:
        team_card_markup.add(types.InlineKeyboardButton(
            text=text_marks,
            callback_data="/c_se -t:{} -r:{}".format(team_id, round_id))
        )
    return team_card_markup


def evaluate_team_inline_keyboard(judge_id, index, archive=False, round_id=None, read_only=False):
    judge = db.get_judge(judge_id)
    assert judge.project is not None

    if not round_id:
        round_id = judge.current_round.id

    evaluate_team_markup = types.InlineKeyboardMarkup(row_width=6)
    criterions = db.fetch_criterions(judge)
    prev_b, next_b = False, False

    if index + 1 < len(criterions):
        next_b = True
    if index > 0:
        prev_b = True

    temp = []
    buttons = []

    icon = B_HISTORY
    cmd_prev = "/c_ap -c:{} -r:{}".format(index - 1, round_id)
    cmd_next = "/c_an -c:{} -r:{}".format(index + 1, round_id)
    cmd_finish = "/c_fa -t:{}".format(judge.project.id)

    if not (archive or read_only):
        assignment = db.get_assignment(judge, judge.project)
        if assignment.rate_scale != 0:
            score_range = assignment.rate_scale
        else:
            score_range = judge.rate_scale

        for i in range(score_range + 1):
            temp.append(
                types.InlineKeyboardButton(
                    text=str(i), callback_data="/c_ssc -s:{} -t:{} -c:{}".format(i, judge.project.id, index)
                )
            )
        evaluate_team_markup.add(*temp)
        cmd_prev = "/c_ep -c:{}".format(index - 1)
        cmd_next = "/c_en -c:{}".format(index + 1)
        cmd_finish = "/c_fe -t:{}".format(judge.project.id)
        icon = B_SEARCH

    if read_only:
        cmd_prev = "/c_ep -c:{}".format(index - 1)
        cmd_next = "/c_en -c:{}".format(index + 1)
        cmd_finish = "/c_fe -t:{}".format(judge.project.id)
        icon = B_SEARCH

    if prev_b:
        buttons.append(
            types.InlineKeyboardButton(text=B_PREW, callback_data=cmd_prev)
        )

    label = db.get_lang_obj(judge.lang.code)
    if archive:
        but_finish = label.archive_pass
    else:
        but_finish = label.evaluate_pass
    buttons.append(types.InlineKeyboardButton(text=but_finish,
                                              callback_data=cmd_finish))

    if next_b:
        buttons.append(
            types.InlineKeyboardButton(text=B_NEXT, callback_data=cmd_next)
        )

    evaluate_team_markup.add(*buttons)
    team_name = "{} {}".format(judge.project.name, icon)

    return (team_name, judge.criterion.title, evaluate_team_markup,
            criterions[index].description, len(criterions), judge.criterion.criterion_group.title)


def create_settings_mrk(text_scale, text_lang):
    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(types.InlineKeyboardButton(text=text_scale,
                                          callback_data="/c_sse"),
               types.InlineKeyboardButton(text=text_lang,
                                          callback_data="/c_lse"))
    return markup


def language_select_mrk():
    markup = types.InlineKeyboardMarkup(row_width=2)
    language_list = db.fetch_all_langs()
    buttons = []
    for lang in language_list:
        buttons.append(types.InlineKeyboardButton(text=lang.name.capitalize(),
                                                  callback_data="/c_sl -v:{}".format(lang.id)))
    markup.add(*buttons)
    markup.add(
        types.InlineKeyboardButton(text=B_PREW,
                                   callback_data="/c_sm")
    )
    return markup


def create_rounds_mrk(judge_id):
    rounds_list = db.fetch_rounds(judge_id)
    markup = types.InlineKeyboardMarkup(row_width=1)
    for rnd in rounds_list:
        markup.add(
            types.InlineKeyboardButton(text=rnd.title, callback_data="/c_sr -n:{}".format(rnd.id)))
    return markup


def create_confirm_markup(project_id, b_conf, b_dec):
    markup = types.InlineKeyboardMarkup(row_width=2)
    markup.add(
        types.InlineKeyboardButton(text=B_DECLINE.format(b_dec),
                                   callback_data="/c_de -t:{}".format(project_id)),
        types.InlineKeyboardButton(text=B_CONFIRM.format(b_conf),
                                   callback_data="/c_ce -t:{}".format(project_id))
    )
    return markup


def create_confirm_modif_markup(project_id, b_conf, b_dec):
    markup = types.InlineKeyboardMarkup(row_width=2)
    markup.add(
        types.InlineKeyboardButton(text=B_DECLINE.format(b_dec),
                                   callback_data="/c_de -t:{}".format(project_id)),
        types.InlineKeyboardButton(text=B_CONFIRM.format(b_conf),
                                   callback_data="/c_cm -t:{}".format(project_id))
    )
    return markup


if __name__ == '__main__':
    pass