#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

from django.db import models
from parler.models import TranslatableModel, TranslatedFields

from core.data.settings import State

# projects


class Competition(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )

    class Meta:
        db_table = 'projects_competition'

    def __str__(self):
        return self.title


class Project(models.Model):
    name = models.TextField(null=False)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    title = models.TextField(null=False)
    region = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'projects_project'


class Participant(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    name = models.TextField(null=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'projects_participant'


# workloads

class Round(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )
    start_timestamp = models.DateTimeField()
    end_timestamp = models.DateTimeField()
    filter = models.BooleanField()

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'workloads_round'


# criterions

class Rubric(TranslatableModel):
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'criterions_rubric'


class CriterionGroup(TranslatableModel):
    rubric = models.ForeignKey(Rubric, on_delete=models.CASCADE)
    translations = TranslatedFields(
        title=models.CharField(max_length=200)
    )
    order = models.IntegerField(null=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'criterions_criteriongroup'


class Criterion(TranslatableModel):
    criterion_group = models.ForeignKey(CriterionGroup, on_delete=models.CASCADE)
    translations = TranslatedFields(
        description=models.CharField(max_length=400),
        title=models.CharField(max_length=200)
    )
    order = models.IntegerField(null=False)
    score = models.SmallIntegerField(null=False)
    read_only = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'criterions_criterion'


# judges


class Language(models.Model):
    name = models.TextField()
    code = models.CharField(max_length=4)
    default = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'judges_language'


class Judge(models.Model):
    tel_id = models.IntegerField(null=True)
    full_name = models.CharField(max_length=40)
    lang = models.ForeignKey(Language, on_delete=models.CASCADE, null=True, blank=True)
    token = models.CharField(max_length=15, unique=True)
    state = models.SmallIntegerField(default=State.NULL.value, blank=True)
    criterion = models.ForeignKey(Criterion, null=True, on_delete=models.CASCADE, blank=True)
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE, blank=True)
    rate_scale = models.SmallIntegerField(default=5)
    last_message = models.IntegerField(default=0, null=True, blank=True)
    current_round = models.ForeignKey(Round, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return self.full_name

    class Meta:
        db_table = 'judges_judge'


class Assignment(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    judge = models.ForeignKey(Judge, on_delete=models.CASCADE)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    rate_scale = models.SmallIntegerField(default=0)
    last_modified_ts = models.DateTimeField(null=True, blank=True)
    modified = models.BooleanField(default=False)
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return 'Assignment: {} to {}'.format(self.project.name, self.judge)

    class Meta:
        db_table = 'workloads_assignment'


class Media(models.Model):
    judge = models.ForeignKey(Judge, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    round = models.ForeignKey(Round, on_delete=models.CASCADE)
    media_id = models.TextField()
    media_type = models.CharField(max_length=5)

    def __str__(self):
        return str(self.tel_id)

    class Meta:
        db_table = 'judges_media'


# marks

class Mark(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    criterion = models.ForeignKey(Criterion, on_delete=models.CASCADE)
    mark = models.FloatField(null=False)

    def __str__(self):
        return str(self.assignment)

    class Meta:
        db_table = 'marks_mark'


class Labels(TranslatableModel):
    translations = TranslatedFields(
        msg_start=models.TextField(),
        msg_welcome=models.TextField(),
        msg_token_request=models.TextField(),
        msg_wrong_token=models.TextField(),
        msg_team_select=models.TextField(),
        msg_no_available_teams=models.TextField(),
        msg_project_card=models.TextField(),
        but_go_to_marks=models.TextField(),
        msg_no_voice=models.TextField(),
        msg_no_photo=models.TextField(),
        msg_no_video=models.TextField(),
        but_cancel=models.TextField(),
        evaluate_prev=models.TextField(),
        evaluate_next=models.TextField(),
        evaluate_pass=models.TextField(),
        settings=models.TextField(),
        rate_scale=models.TextField(),
        usr_language=models.TextField(),
        msg_already_logged_in=models.TextField(),
        msg_current_mark=models.TextField(),
        msg_none=models.TextField(),
        msg_select_round=models.TextField(),
        but_confirm=models.TextField(),
        but_decline=models.TextField(),
        msg_confirm_eval_start=models.TextField(),
        msg_you_need_to_finish=models.TextField(),
        msg_you_cant_modify_anymore=models.TextField(),
        msg_confirm_modif_start=models.TextField(),
        msg_you_have_finished=models.TextField(),
        msg_your_media_was_saved=models.TextField(),
        archive_pass=models.TextField(),
        msg_marks_are_empty=models.TextField(),
        no_current_round=models.TextField()
    )

    def get_model_fields(self):
        return self._parler_meta._fields_to_model.keys()

    def __str__(self):
        return str(self.msg_start)

    class Meta:
        db_table = 'labels_labels'


"""
    Transactions
    Table that stores all the actions made by the users
    id|judge|initial_state|resulting_state|project|criterion|media_action|media|api_response|value|timestamp
"""


class Transactions(models.Model):
    judge = models.ForeignKey(Judge, on_delete=models.DO_NOTHING)
    initial_state = models.IntegerField(default=State.NULL.value)
    resulting_state = models.IntegerField(blank=True)
    project = models.ForeignKey(Project, on_delete=models.DO_NOTHING, blank=True)
    criterion = models.ForeignKey(Criterion, on_delete=models.DO_NOTHING, blank=True)
    api_response = models.TextField(blank=True)
    value = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'events_transaction'
