import os

from core.data.settings import DEF_LANG

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get('RATE_BOT_DB_NAME'),
        'USER': os.environ.get('RATE_BOT_DB_USER'),
        'PASSWORD': os.environ.get('RATE_BOT_DB_PASSWORD'),
        'HOST': os.environ.get('RATE_BOT_DB_HOST'),
        'PORT': os.environ.get('RATE_BOT_DB_PORT'),
    }
}


INSTALLED_APPS = (
    'core',
)

SECRET_KEY = 'SECRET'

LANGUAGE_CODE = DEF_LANG

DEBUG = True
