v. 0.1.0a
Changelog:
1. requirements.txt added
2. Database module created (database.py)
3. 2 tests for database module (test/test_database.py)
4. Config created (as)

v 0.1.0b
Changelog:
1. Table 'users' renamed on 'judges'
2. Added parameter 'both' in execute function
3. database.is_token_duplicate function added
4. database.add_judge function added 
4. Test of add_judge function
5. database.get_state_by_tok function added

v 0.1.0c
Changelog:
1. Created bot.py
2. Added States class in config.py
3. Added 'bot' and 'proxy' settings for the bot in config.py

v 0.1.1
Changelog:
1. Split on micro-services
2. Database module cleaned
3. config renamed onto settings
4. Added core service
5. Added main_receiver

v 0.1.2
Changelog:
1. auth_microservice created
2. database module moved to core
3. settings deleted from auth_microservice
4. auth_service/bot use core/settings
5. database settings was changed
! TO-DO: Database tests
! Text managment tool

v 0.1.3
Changelog:
1. Multi lang support for labels
2. Database new methods added
 - fetch_labels
 - get_user_lang
3. DEF_LANG setting added in core/settings.py

v 0.1.3a
Changelog:
1. Auth system ready

v 0.1.3b
Changelog:
1. HTML type for messages.
2. Team table for judge (raw)
3. Lang modified field
4. msg_welcome field added to labels

v 0.1.4
Changelog:
1. Team table for judges finished
! Current limit for teams count is 72
2. msg_not_available_teams found field added to labels
3. MAIN_MENU_MARKUP constant created

v 0.2.0
Changelog:
1. Added evaluation_microservice. It handle all the requests for evaluation of teams.
2. Messages are wrapped in the additional layer:
'cmd_wrapper = {"command": cmd_name,
                "message": message}'
3. callbacks are updated to understand the required set of commands:
    auth_microservice:
    - no commands
    evaluation_microservice:
    + send_team_select (sends team selection menu)
4. database.fetch_teams_for_judge() updated to return only projects of current round

v 0.2.1
Changelog:
1. Added team evaluation dialog start by /cmd_team_eval -n:<id>
2. Added detailed team table for judges

v 0.2.1a
Changelog:
1. Huge code refactor
2. Created module markup in core
! All the keyboards are currently called from markup

v 0.2.1b
Changelog:
1. Team select keyboard finished
2. cmd_wrapper extended with a "call" key.
'cmd_wrapper = {"command": None,
               "message": None,
               "call": None}'
3. main_polling handles the callback_queries

v 0.2.1c
Changelog:
1. Fixed auth microservice

v 0.2.2
Changelog:
1. Project cards created
2. - 'cmd_eval_team'
      renamed to
   + 'cmd_show_project_card'
3. fetch_project_card_data() function added to database.py
4. added new labels:
    + msg_project_card
    + but_go_to_marks
5. Buttons icons for project cards added to markup:
    + B_SEND_AUDIO
    + B_SEND_VIDEO
    + B_SEND_PHOTO
6. New callback commands added:
    + send_audio
    + send_photo
    + send_video
    + start_eval
7. Redundant flag 'with_welcome' removed from send_team_select

v 0.2.2a
Changelog:
1. get_user_lang moved to the database file to be accessible from all the microservices

v 0.2.3
Changelog:
1. media_microservice added
Supports:
   ++ get_voice
   ++ get_photo
   ++ get_video
2. Fixed bug with empty team list from database module
3. Added new type of handler in main_polling
    content_type = voice, video, photo
4. New State was added:
   EVAL_MODE = 12
5. State_prefix field was added to the judge
6. set_state_prefix func was added to the database

v 0.3.0
Changelog:
1. fetch_criterion_by_id added in database
2. Added last_msg, last_criterion to judge model

v 0.3.1
Changelog:
1. Added 4 functions to database:
    fix_rate_scale()
    update_assignment_scale()
    fetch_assignment_rate_scale()
    fetch_current_round_id()
    get_judge_rate_scale()
2. Rate scale now is fixed when judge starts first time assignment for concrete project

v 0.4.0
Changelog:
1. Huge refactor:
    - New state list:
        10 INIT_STATE
        12 ARCHIVE_STATE
        13 SETTINGS_STATE
        20 EVALUATION STATE
    - New micro-services list:
        - auth
        - main_menu
        - evaluation
        - archive
        - settings
        - media
    - Callback commands refactored:
        CMD_MENU_SET:
            "cmd_select_team_range",
            "cmd_show_project_card",
            "cmd_select_team_range",
            "cmd_finish_eval"
        CMD_EVALUATION_SET:
            "cmd_evaluate_next",
            "cmd_evaluate_prev",
            "cmd_set_score"
        CMD_ARCHIVE_SET:
            "cmd_next",
            "cmd_prev"
        CMD_SETTINGS_SET:
            "cmd_scale_settings",
            "cmd_lang_settings",
            "cmd_set_scale",
            "cmd_set_lang"
        CMD_MEDIA_SET:
            "cmd_get_voice",
            "cmd_get_photo",
            "cmd_get_video"
    - New rules added to the main_polling for state management

v 0.4.0a
Changelog:
1. Exchange package changed: self._channel to self.channel to use in the another modules (channel.basic_publish)
2. send_welcome moved from evaluation service to the auth_microservice
3. Small code style changes

v 0.4.0b
Changelog:
1. Added new rules in main_polling to manage:
    - settings microservice
    - archive microservice

v 0.4.0b
Changelog:
1. routing key in auth_microservice updated
2. Updated commands in markup
3. Added new commands in settings

v 0.4.1
Changelog:
1. Created settings microservice
2. Created main_menu_microservice
3. added new label:
    + msg_already_logged_in
4. auth/bot/notify_evaluation_microservice renamed to notify_menu_service

v 0.4.1a
Changelog:
1. added new labels:
    + msg_current_mark,
    + msg_none
2. evaluation service small refactor
3. Added notify_menu_service to evaluation service

v 0.4.1b
Changelog:
1. get_mark, set_mark, get_assignment moved in database file
2. evaluate_team_inline_keyboard adapted for the archive purpose

v 0.5.0
Changelog:
1. Archive micro service created:
    - New command set
        CMD_ARCHIVE_SET:
            "cmd_show_archive",
            "cmd_archive_prev",
            "cmd_archive_next",
            "cmd_select_round",
            "cmd_show_archive_card",
            "cmd_select_archive_range",
            "cmd_finish_archive"

2. Main polling:
    - Callback handler moved to the top
    - Start and token handlers moved to the bottom
    - new rules in callback hanlder added
3. Markup:
    - new markup create_rounds_mrk
    - get_detailed_team_select_keyboard new argument added (archive=False)
    - get_project_card_inline_keyboard new argument added (archive=False)
    - evaluate_team_inline_keyboard new argument added (archive=False)
4. Database:
    - fetch_teams_for_judge_by_round created to allow getting teams in the round by id
    - fetch_round_title used to get round title by id
    - fetch_rounds used to get all the rounds

v 0.5.0a
Changelog:
1. REFRESH button handler added to the settings microservice
2. REFRESH button handler added to the main_menu microservice

v 0.5.1
Changelog:
1. Evaluation sysetem refactor:
    - last_modified_ts field added to assignment table
    - modified boolean field added to assignment table
    - To finish evaluation session judge need to press "Finish"
    - If there is any unrated mark criteria judge can't leave this evaluation session
2. Activation of evaluation mode is abstract database function
3. create_confirm_markup added to markup
4. added new labels:
    + but_confirm
    + but_decline
    + msg_confirm_eval_start
5. Now Criterion id fetched from the callback_data, removes one db request in archive and evaluation
6. Judge can't leave till evaluation properly finished.
All text messages would lead to resending the last criteria and erasing prev method.
7. settings.MIN_TO_MODIFY parameter added

v 0.5.1a
Changelog:
1. bot.delete_message fixed
2. After confirmation of rules main_menu.send_confirmation the rules message deleted
3. After sending media the last VISITED criteria evaluation message will be send

v 0.5.2
Changelog:
1. Refresh button handling in evaluation
2. Bug with wrong message id in main_menu fixed
3. Main menu markup now will be sent with resend_evaluation

v 0.6.0
Changelog:
1. Updated core/exchange
2. Created core/publisher
3. Created core/consumer
4. Added icon in team_name for eval and archive mode

v 0.6.1
Changelog:
1. msg_confirm_modif_start added to labels
2. cmd_confirm_modificaiton command added
3. Flag for proxy use set to False
4. current_round added to judges_judge table
5. round_id added to judges_media table
6. Huge update of database.py
    - improved speed
    - execute function changed

v 0.6.1a
Changelog:
1. Many bugs was fixed

v 0.6.1b
Changelog:
1. RB-64: Commands fix. All commands now became shorter.
2. RB-67: Next, prew -> arrows
3. RB-66: Media bug fixed
4. RB-63: Deleting keyboard after finish
5. RB-66: Go to first unset criteria
6. RB-61: Default language criteria

v 0.6.1c
Changelog:
1. RB-62.
2. parse mode change to "markdown"
3. msg_your_media_was_saved label added.

v 0.6.2
Changelog:
1. Fixed rate scale on modification is equal to rate scale on evaluation

v 0.6.2a
Changelog:
1. Fixed media bug in archive
2. RB-73 Fixed
3. Added icons to team_select card
4. Updating of message in archive instead of resending (c_fa-cmd_finish_archive)
5. Bug with empty ratescale in archive fixed

v 0.7.0
Changelog:
1. RB-83. Criteria Group Title added to eval and archive
2. RB-82. Message about empty marks in archive
3. RB-80. Settings now is one-window application
4. RB-79. Added sign about default settings in welcome message
5. RB-75.

v 0.7.0
Changelog:
1. Database in case of failed connection tries to reconnect.

v 0.7.0
Changelog:
1. Database code segmentation added
2. Auth micro service comments added

v 0.7.0
Changelog:
1. New settings format
2. start-local-server.sh renamed to start-prod-server.sh
3. README file created