FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./main_menu/ /src/main_menu/
CMD [ "python", "./main_menu/bot.py" ]
