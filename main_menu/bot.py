#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core.data import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["command"] == "send_team_select":
        if body["call"]:
            judge_id = body["call"].from_user.id
        else:
            judge_id = body["message"].from_user.id
        db.set_state(judge_id, s.State.MENU.value)
        send_team_select(judge_id)
    elif body["command"] == "send_project_card":
        judge_id = body["call"].from_user.id
        project_id, round_id = body["args"]
        project_obj = db.get_project_by_id(project_id)
        round_obj = db.get_round_by_id(round_id)
        if project_obj and round_obj:
            send_project_card(judge_id, project_id)
        else:
            send_error_card(judge_id)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


@bot.message_handler(content_types=["text"])
def main_menu_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_SEARCH:
        if db.set_state(judge_id, new_state=s.State.MENU.value):
            send_team_select(judge_id)
        else:
            send_error_card(judge_id)
    elif message.text == mrk.B_REFRESH:
        send_team_select(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge = db.get_judge(call.from_user.id)
    cur_round = db.fetch_current_round(judge.lang.code)
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_spc":
        judge.current_round = cur_round
        judge.save()
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        send_project_card(judge.tel_id, project_id)
    elif command == "c_str":
        label = db.get_lang_obj(judge.lang.code)
        bot.answer_callback_query(call.id)
        team_ids = re.findall('\d+', call.data)
        teams_data = db.fetch_projects_data_by_ids(team_ids, judge.tel_id)
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams_data)
        msg_text = label.msg_team_select.format(mrk.B_SEARCH, cur_round.title, "")
        bot.send_message(chat_id=judge.tel_id,
                         text=msg_text,
                         reply_markup=mrk_team_response["markup"],
                         parse_mode="markdown")
    elif command == "c_se":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        eval_status = db.project_eval_status(judge.tel_id, project_id)
        bot.answer_callback_query(call.id)
        if eval_status == 0:
            send_confirmation(judge.tel_id, project_id)
        elif eval_status == 1:
            send_confirm_modification(judge.tel_id, project_id)
        else:
            label = db.get_lang_obj(judge.lang.code)
            bot.answer_callback_query(call.id, text=label.msg_you_cant_modify_anymore, show_alert=True)
            judge.state = s.State.ARCHIVE.value
            judge.curr_round = db.fetch_current_round()
            judge.save()
            notify_archive(call, project_id)
    elif command == "c_de":
        bot.answer_callback_query(call.id)
        send_team_select(judge.tel_id)


def send_project_card(user_id, project_id):
    judge = db.get_judge(user_id)
    label = db.get_lang_obj(judge.lang.code)
    project_data = db.fetch_project_card_data(project_id, judge.lang.code)
    if project_data:
        curr_round = db.fetch_current_round(judge.lang.code)
        project_card_markup = mrk.get_project_card_inline_keyboard(
            project_id, label.but_go_to_marks, round_id=curr_round.id
        )
        bot.send_message(chat_id=user_id,
                         text=label.msg_project_card.format(**project_data),
                         reply_markup=project_card_markup,
                         parse_mode="markdown")
    else:
        send_error_card(judge.tel_id)


def send_team_select(user_id):
    db.set_state(user_id, s.State.MENU.value)
    judge = db.get_judge(user_id)

    label = db.get_lang_obj(judge.lang.code)
    cur_round = db.fetch_current_round(judge.lang.code)
    if not cur_round:
        bot.send_message(user_id, label.no_current_round)
        return
    teams = db.fetch_teams_for_judge(user_id)
    if teams:

        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams)
        if mrk_team_response["small"]:
            msg_text = label.msg_team_select.format(mrk.B_SEARCH, cur_round.title, "")
        else:
            msg_text = label.msg_team_select.format(mrk.B_SEARCH, cur_round.title, mrk_team_response["description"])
        bot.send_message(user_id,
                         text=msg_text,
                         parse_mode="markdown",
                         reply_markup=mrk_team_response["markup"])
    else:
        bot.send_message(user_id,
                         parse_mode="markdown",
                         text=label.msg_no_available_teams,
                         reply_markup=mrk.MAIN_MENU_MARKUP)


def send_confirmation(judge_id, project_id):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    markup = mrk.create_confirm_markup(project_id, label.but_confirm, label.but_decline)
    msg = bot.send_message(judge_id,
                           label.msg_confirm_eval_start,
                           reply_markup=markup,
                           parse_mode="markdown")
    judge.last_message = msg.message_id
    judge.save()


def send_confirm_modification(judge_id, project_id):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    markup = mrk.create_confirm_modif_markup(project_id, label.but_confirm, label.but_decline)
    msg = bot.send_message(judge_id,
                           label.msg_confirm_eval_start,
                           reply_markup=markup,
                           parse_mode="markdown")
    judge.last_message = msg.message_id
    judge.save()


def send_error_card(judge_id):
    bot.send_message(judge_id, 'Error Placeholder')


def notify_archive(call, team_id):
    body = pickle.dumps(
        {
            "command": "show_archive -t:{}".format(team_id),
            "message": None,
            "call": call
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="archive",
        body=body
    )


if __name__ == '__main__':
    con = PickleConsumer(s.RMQ, 5672, 'main_menu', callback, exchange='rate_bot', logger='main_menu',
                         username=s.RMQ_USER, password=s.RMQ_PASS)
    try:
        logger.log(logging.DEBUG, '[MAIN_MENU] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

