#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core.data import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    """
        Message receiver
        Inner commands:
        'show_archive' - requests archive for the team

    :param consumer:
    :param body:
    :return:
    """
    if body["command"] and "show_archive" in body["command"]:
        project_id = int(re.search('(?<=-t.)\w+', body["command"]).group(0))
        judge = db.get_judge(body["call"].from_user.id)
        db.update_last_criterion(judge.tel_id, 0)
        judge.project = db.get_project_by_id(project_id)
        judge.state = s.State.ARCHIVE.value
        judge.save()
        send_archive_message(judge.tel_id, 0)
    elif body["command"] == "send_project_card":
        judge_id = body["call"].from_user.id
        project_id, round_id = body["args"]
        judge = db.get_judge(body["call"].from_user.id)
        judge.state = s.State.ARCHIVE.value
        judge.project = db.get_project_by_id(project_id)
        judge.current_round = db.get_round_by_id(round_id)
        judge.save()
        send_project_card(judge_id, project_id, round_id=round_id)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'archive', callback, exchange='rate_bot', logger='archive', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def main_menu_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_HISTORY:
        db.set_state(judge_id, s.State.ARCHIVE.value)
        send_round_selector(judge_id)
    elif message.text == mrk.B_REFRESH:
        send_round_selector(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge = db.get_judge(call.from_user.id)
    label = db.get_lang_obj(judge.lang.code)
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_sr":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-n.)\w+', call.data).group(0))
        judge.current_round = db.get_round_by_id(round_id)
        judge.save()
        send_team_select(judge.tel_id, round_id)
    elif command == "c_sac":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        judge.project = db.get_project_by_id(project_id)
        send_project_card(judge.tel_id, project_id, round_id)
    elif command == "c_str":
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        data = call.data.split("-r")[0]
        team_ids = re.findall('\d+', data)
        teams_data = db.fetch_projects_data_by_ids(team_ids, judge.tel_id)
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams_data, archive=True, round_id=round_id)
        msg_text = label.msg_team_select.format(mrk.B_HISTORY, db.fetch_current_round(judge.lang.code), "")
        bot.send_message(chat_id=judge.tel_id,
                         text=msg_text,
                         reply_markup=mrk_team_response["markup"],
                         parse_mode="markdown")
    elif command == "c_sa":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        project = db.get_project_by_id(project_id)
        judge.project = project
        judge.save()
        if db.project_was_evaluated(judge.tel_id, project_id, round_id):
            bot.answer_callback_query(call.id)
        else:
            bot.answer_callback_query(call.id, text=label.msg_marks_are_empty)
        db.update_last_criterion(judge.tel_id, 0)
        send_archive_message(judge.tel_id, 0, round_id=round_id)
    elif command == "c_an":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        db.update_last_criterion(judge.tel_id, criterion_number)
        send_archive_message(judge.tel_id, criterion_number, last_msg_id=judge.last_message, round_id=round_id)
    elif command == "c_ap":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        db.update_last_criterion(judge.tel_id, criterion_number)
        send_archive_message(judge.tel_id, criterion_number, last_msg_id=judge.last_message, round_id=round_id)
    elif command == "c_fa":
        bot.answer_callback_query(call.id)
        send_round_selector(judge.tel_id, last_msg=judge.last_message)


def send_project_card(judge_id, project_id, round_id=None):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    project_data = db.fetch_project_card_data(project_id)
    project_card_markup = mrk.get_project_card_inline_keyboard(project_id, label.but_go_to_marks,
                                                               archive=True, round_id=round_id)
    bot.send_message(chat_id=judge_id,
                     text=label.msg_project_card.format(**project_data),
                     reply_markup=project_card_markup,
                     parse_mode="markdown")


def send_round_selector(judge_id, last_msg=False):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    rounds_markup = mrk.create_rounds_mrk(judge_id)
    msg_my_archive = mrk.B_HISTORY + label.msg_select_round
    if last_msg:
        bot.edit_message_text(chat_id=judge_id,
                              message_id=last_msg,
                              text=msg_my_archive,
                              parse_mode="markdown",
                              reply_markup=rounds_markup)
    else:
        bot.send_message(judge_id,
                         text=msg_my_archive,
                         parse_mode="markdown",
                         reply_markup=rounds_markup)


def send_archive_message(judge_id, criterion_number, last_msg_id=None, round_id=None):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    current_mark = db.get_mark(judge, criterion_num=criterion_number, archive=True)
    temp_data = mrk.evaluate_team_inline_keyboard(judge_id, criterion_number, archive=True, round_id=round_id)
    msg_text = "*{}\n{}\n{} ({}/{})*\n\n{}\n\n*{}: {}*".format(
        temp_data[0], temp_data[5], temp_data[1], criterion_number + 1, temp_data[4],
        temp_data[3], label.msg_current_mark,
        current_mark if current_mark is not None else label.msg_none
    )
    if last_msg_id is None:
        sent_message = bot.send_message(
            text=msg_text,
            chat_id=judge_id,
            parse_mode="markdown",
            reply_markup=temp_data[2]
        )
        judge.last_message = sent_message.message_id
        judge.save()
    else:
        bot.edit_message_text(
            text=msg_text,
            chat_id=judge_id,
            message_id=last_msg_id,
            reply_markup=temp_data[2],
            parse_mode="markdown"
        )


def send_team_select(judge_id, round_id):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    cur_round = db.get_round_by_id(round_id)
    teams = db.fetch_teams_for_judge(judge_id, cur_round)
    if teams:
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams, archive=True, round_id=round_id)
        if mrk_team_response["small"]:
            msg_text = label.msg_team_select.format(mrk.B_HISTORY, cur_round.title, "")
        else:
            msg_text = label.msg_team_select.format(mrk.B_HISTORY, cur_round.title, mrk_team_response["description"])
        bot.send_message(judge_id,
                         text=msg_text,
                         parse_mode="markdown",
                         reply_markup=mrk_team_response["markup"])
    else:
        bot.send_message(judge_id,
                         parse_mode="markdown",
                         text=label.msg_no_available_teams,
                         reply_markup=mrk.MAIN_MENU_MARKUP)


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[ARCHIVE] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
