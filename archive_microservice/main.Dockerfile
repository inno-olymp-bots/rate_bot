FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./archive_microservice/ /src/archive_microservice/
CMD [ "python", "./archive_microservice/bot.py" ]
