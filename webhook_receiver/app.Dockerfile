FROM mdmxfry/python

EXPOSE 443
EXPOSE 8443

RUN openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout /src/webhook_pkey.pem -out /src/webhook_cert.pem -subj '/C=RU/ST=Tatarstan/L=Kazan/O=Inno/CN=$WEBHOOK_HOST'

ADD  ./core/ /src/core/
ADD  ./webhook_receiver/ /src/webhook_receiver/
CMD [ "python", "./webhook_receiver/main_webhook.py" ]
