#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import time

import flask
import telebot
from telebot import apihelper

from core import markup
from core.data.settings import *
from core.publisher import PicklePublisher
from core.database import get_current_judge_state as gcs

logger = create_logger('webhook')


def listener(messages):
    for m in messages:
        if m.content_type == "text":
            logger.info(str(m.chat.first_name) + " [" + str(m.chat.id) + "]: " + m.text)


if USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**PROXY)}

bot = telebot.TeleBot(TOKEN, num_threads=4)
if LOG:
    bot.set_update_listener(listener)


queues = ['auth', 'main_menu', 'evaluation', 'media', 'archive', 'settings']

pub = PicklePublisher(RMQ, 5672, queues, exchange='rate_bot', logger='polling', username=RMQ_USER, password=RMQ_PASS)

cmd_wrapper = {"command": None,
               "message": None,
               "call": None}

app = flask.Flask(__name__)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    target_queue = None
    w_call = cmd_wrapper.copy()
    w_call["call"] = call
    command = re.search('(?<=/)\w+', call.data).group(0)
    judge_id = call.from_user.id
    if command in CMD_MENU_SET and gcs(judge_id) == State.MENU.value:
        target_queue = "main_menu"
    elif (command in CMD_EVALUATION_SET and gcs(judge_id) == State.EVALUATION.value
          or command == "c_ce" and gcs(judge_id) == State.MENU.value
          or command == "c_cm" and gcs(judge_id) == State.MENU.value):
        target_queue = "evaluation"
    elif command in CMD_ARCHIVE_SET and gcs(judge_id) == State.ARCHIVE.value:
        target_queue = "archive"
    elif command in CMD_SETTINGS_SET and gcs(judge_id) == State.SETTINGS.value:
        target_queue = "settings"
    elif command in CMD_MEDIA_SET:
        target_queue = "media"

    if target_queue:
        pub.publish_message(
            w_call,
            target_queue
        )


@bot.message_handler(func=lambda message:
gcs(message.from_user.id) == State.EVALUATION.value,
                     content_types=["text"])
def eval_text_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "evaluation"
    )


@bot.message_handler(func=lambda message:
message.text == markup.B_SEARCH
or (message.text == markup.B_REFRESH
    and gcs(message.from_user.id) == State.MENU.value)
                     )
def menu_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "main_menu"
    )


@bot.message_handler(func=lambda message:
gcs(message.from_user.id) == State.EVALUATION.value,
                     content_types=["voice", "video", "photo"])
def media_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "media"
    )


@bot.message_handler(func=lambda message:
message.text == markup.B_SETTINGS
or (message.text == markup.B_REFRESH
    and gcs(message.from_user.id) == State.SETTINGS.value))
def settings_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "settings"
    )


@bot.message_handler(func=lambda message:
message.text == markup.B_HISTORY
or (message.text == markup.B_REFRESH
    and gcs(message.from_user.id) == State.ARCHIVE.value))
def archive_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "archive"
    )


@bot.message_handler(commands=["start"])
def start_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "auth"
    )


@bot.message_handler(func=lambda message: gcs(message.from_user.id) == State.NULL.value)
def token_handler(message):  # token as expected in this case.
    w_message = cmd_wrapper.copy()  # However, it handles any text from user
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "auth"
    )


bot.remove_webhook()
time.sleep(0.1)
bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH,
                certificate=open(WEBHOOK_SSL_CERT, 'r'))


try:
    pub.run()
    app.run(host=WEBHOOK_LISTEN,
            port=WEBHOOK_PORT,
            ssl_context=(WEBHOOK_SSL_CERT, WEBHOOK_SSL_PRIV),
            debug=False)
    raise KeyboardInterrupt
except KeyboardInterrupt:
    pub.terminate()
