#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import os
import json

import pytest
import telebot

from auth_microservice import bot

with open('tests/types/message.txt') as msg_txt:
    message_str = msg_txt.read()

msg_params = {
    'content_type': 'text',
    'message_id': None,
    'user_id': None,
    'date': None,
    'user_last_name': None,
    'user_first_name': None,
    'user_username': None
}


class TestAuthToken(object):
    START_ID = 100
    keys = msg_params.copy()
    keys['message_id'] = 10259
    keys['user_id'] = 89689285
    keys['date'] = 1553073686
    keys['user_last_name'] = 'Fry'
    keys['user_first_name'] = 'Max',
    keys['user_username'] = 'mdmxfry1'
    keys['text'] = None

    user = telebot.types.User(
        first_name=keys['user_first_name'],
        last_name=keys['user_last_name'],
        id=keys['user_id'],
        is_bot=False
    )
    chat = telebot.types.Chat(
        id=keys['user_id'],
        type='private'
    )

    @pytest.fixture(scope="function", autouse=True)
    def erase_user(self, request):

        bot.db._drop_users_gte_x(self.START_ID)

        request.addfinalizer(lambda: bot.db._drop_users_gte_x(self.START_ID))

    def create_msg(self):
        msg_dict = json.loads(message_str % self.keys)
        msg_dict['from_user'] = self.user
        msg_dict['chat'] = self.chat
        return telebot.types.Message(**msg_dict)

    def create_user(self):
        token = bot.db._create_token('Ильдар Тесторов', self.START_ID)
        self.START_ID += 1
        return token

    def test_no_token(self):
        self.keys['text'] = '/start'
        msg = self.create_msg()
        bot.start(msg)

        assert True

    def test_wrong_token(self):
        self.keys['text'] = '/start WrongToken'
        msg = self.create_msg()
        bot.start(msg)

        assert True

    def test_correct_token(self):
        token = self.create_user()
        self.keys['text'] = '/start ' + token
        msg = self.create_msg()
        bot.start(msg)

        assert True

    def test_manual_token_entry_wrong(self):
        token = self.create_user()

        self.keys['text'] = 'WrongToken'
        msg = self.create_msg()

        bot.start(msg)

    def test_manual_token_entry_true(self):
        token = self.create_user()

        self.keys['text'] = token
        msg = self.create_msg()

        bot.start(msg)
