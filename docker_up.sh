#!/usr/bin/env bash

docker-compose -f ../rate_bot_web/docker_config/compose-helpers.yml down
docker-compose -f ../rate_bot_web/docker_config/compose-local.yml down
docker-compose -f ../rate_bot/docker_config/compose-testdep-microservices.yml down

docker container prune -f
docker rmi $(docker images -f "dangling=true" -q)

docker-compose -f ../rate_bot_web/docker_config/compose-helpers.yml up -d --build
docker-compose -f ../rate_bot_web/docker_config/compose-local.yml up -d --build
docker-compose -f docker_config/compose-test-microservices.yml up -d --build
echo "Completed"

