FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./settings_microservice/ /src/settings_microservice/
CMD [ "python", "./settings_microservice/bot.py" ]
