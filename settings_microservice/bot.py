#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core.data import settings as s
from core.consumer import PickleConsumer


if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'settings', callback, exchange='rate_bot', logger='settings', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def settings_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_SETTINGS or message.text == mrk.B_REFRESH:
        send_settings_menu(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge = db.get_judge(call.from_user.id)
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_sm":
        bot.answer_callback_query(call.id)
        send_settings_menu(judge.tel_id, update=True)
    elif command == "c_sse":
        bot.answer_callback_query(call.id)
        send_settings_scale(judge.tel_id, update=True)
    elif command == "c_lse":
        bot.answer_callback_query(call.id)
        send_settings_lang(judge.tel_id, update=True)
    elif command == "c_ssc":
        new_scale = int(re.search('(?<=-v.)\w+', call.data).group(0))
        judge.rate_scale = new_scale
        judge.save()
        bot.answer_callback_query(
            callback_query_id=call.id,
            text="\U0001F4BE"
        )
        send_settings_scale(judge.tel_id, update=True)
    elif command == "c_sl":
        bot.answer_callback_query(
            callback_query_id=call.id,
            text="\U0001F4BE"
        )
        new_lang = int(re.search('(?<=-v.)\w+', call.data).group(0))
        new_lang = db.get_lang_obj_by_id(new_lang)
        judge.lang = new_lang
        judge.save()
        send_settings_lang(judge.tel_id, update=True)


def send_settings_menu(judge_id, update=False):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    judge.state = s.State.SETTINGS.value
    judge.save()
    msg_text = "*{}*\n{}: 0..{}\n{}: {}".format(
        label.settings, label.rate_scale, judge.rate_scale, label.usr_language, judge.lang.name.capitalize()
    )
    settings_markup = mrk.create_settings_mrk(label.rate_scale, label.usr_language)
    if update:
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=judge.last_message,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=settings_markup
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=settings_markup
        )
        judge.last_message = last_msg.message_id
        judge.save()


def send_settings_scale(judge_id, update=False):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    msg_text = "*{}* 0..{}".format(label.rate_scale, judge.rate_scale)
    if update:
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=judge.last_message,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.SCALE_MARKUP
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.SCALE_MARKUP
        )
        judge.last_message = last_msg.message_id
        judge.save()


def send_settings_lang(judge_id, update=False):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    msg_text = "*{}*: {}".format(label.usr_language, judge.lang.name.capitalize())
    if update:
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=judge.last_message,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.language_select_mrk()
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.language_select_mrk()
        )
        judge.last_message = last_msg.message_id
        judge.save()


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[SETTINGS] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

