import re

import telebot
from telebot import apihelper

from core import markup
from core.data.settings import *
from core.publisher import PicklePublisher
from core.database import get_current_judge_state as gcs


def listener(messages):
    for m in messages:
        if m.content_type == "text":
            print(str(m.chat.first_name) + " [" + str(m.chat.id) + "]: " + m.text)


if USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**PROXY)}

bot = telebot.TeleBot(TOKEN)
bot.set_update_listener(listener)


queues = ['auth', 'main_menu', 'evaluation', 'media', 'archive', 'settings']

pub = PicklePublisher(RMQ, 5672, queues, exchange='rate_bot', logger='polling', username=RMQ_USER, password=RMQ_PASS)


cmd_wrapper = {"command": None,
               "message": None,
               "call": None}


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    target_queue = None
    w_call = cmd_wrapper.copy()
    w_call["call"] = call
    command = re.search('(?<=/)\w+', call.data).group(0)
    judge_id = call.from_user.id
    if command in CMD_MENU_SET and gcs(judge_id) == State.MENU.value:
        target_queue = "main_menu"
    elif (command in CMD_EVALUATION_SET and gcs(judge_id) == State.EVALUATION.value
          or command == "c_ce" and gcs(judge_id) == State.MENU.value
          or command == "c_cm" and gcs(judge_id) == State.MENU.value):
        target_queue = "evaluation"
    elif command in CMD_ARCHIVE_SET and gcs(judge_id) == State.ARCHIVE.value:
        target_queue = "archive"
    elif command in CMD_SETTINGS_SET and gcs(judge_id) == State.SETTINGS.value:
        target_queue = "settings"
    elif command in CMD_MEDIA_SET:
        target_queue = "media"

    if target_queue:
        pub.publish_message(
            w_call,
            target_queue
        )


@bot.message_handler(func=lambda message:
                     gcs(message.from_user.id) == State.EVALUATION.value,
                     content_types=["text"])
def eval_text_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "evaluation"
    )


@bot.message_handler(func=lambda message:
                     message.text == markup.B_SEARCH
                     or (message.text == markup.B_REFRESH
                         and gcs(message.from_user.id) == State.MENU.value))
def menu_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "main_menu"
    )


@bot.message_handler(func=lambda message:
                     gcs(message.from_user.id) == State.EVALUATION.value,
                     content_types=["voice", "video", "photo"])
def media_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "media"
    )


@bot.message_handler(func=lambda message:
                     message.text == markup.B_SETTINGS
                     or (message.text == markup.B_REFRESH
                         and gcs(message.from_user.id) == State.SETTINGS.value))
def settings_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "settings"
    )


@bot.message_handler(func=lambda message:
                     message.text == markup.B_HISTORY
                     or (message.text == markup.B_REFRESH
                         and gcs(message.from_user.id) == State.ARCHIVE.value))
def archive_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "archive"
    )


@bot.message_handler(commands=["start"])
def start_handler(message):
    w_message = cmd_wrapper.copy()
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "auth"
    )


@bot.message_handler(func=lambda message: gcs(message.from_user.id) == State.NULL.value)
def token_handler(message):  # token as expected in this case.
    w_message = cmd_wrapper.copy()  # However, it handles any text from user
    w_message["message"] = message
    pub.publish_message(
        w_message,
        "auth"
    )


if __name__ == '__main__':
    try:
        pub.run()
        bot.remove_webhook()
        bot.polling(none_stop=True)
        raise KeyboardInterrupt
    except KeyboardInterrupt:
        pub.terminate()

