FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./polling_receiver/ /src/polling_receiver/
CMD [ "python", "./polling_receiver/main_polling.py"]
