FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./evaluation_microservice/ /src/evaluation_microservice/
CMD [ "python", "./evaluation_microservice/bot.py" ]
