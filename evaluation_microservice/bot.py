#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core.data import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["command"] and body["command"] == "resend_evaluation":
        resend_evaluation(body["message"].from_user.id, by_last_visited_criterion=True, media=True)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'evaluation', callback, exchange='rate_bot', logger='evaluation', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def text_handler(message):
    judge_id = message.from_user.id
    resend_evaluation(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge = db.get_judge(call.from_user.id)
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_ce":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        db.activate_evaluation_mode(judge.tel_id, project_id)
        send_evaluate_message(judge.tel_id, 0, project_id=project_id)
        bot.delete_message(judge.tel_id, judge.last_message)
    elif command == "c_cm":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        db.set_assignment_modified(judge, project_id)
        db.activate_evaluation_mode(judge.tel_id, project_id, modif=True)
        send_evaluate_message(judge.tel_id, 0, project_id=project_id)
        bot.delete_message(judge.tel_id, judge.last_message)
    elif command == "c_en":
        bot.answer_callback_query(call.id)
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        judge.criterion = db.fetch_criterions(judge)[criterion_number]
        judge.save()
        send_evaluate_message(judge.tel_id, criterion_number, last_msg_id=judge.last_message)
    elif command == "c_ep":
        bot.answer_callback_query(call.id)
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        judge.criterion = db.fetch_criterions(judge)[criterion_number]
        judge.save()
        send_evaluate_message(judge.tel_id, criterion_number, last_msg_id=judge.last_message)
    elif command == "c_ssc":
        bot.answer_callback_query(call.id, text="\U0001F4BE")
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        score = int(re.search('(?<=-s.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        db.set_mark(judge, project_id, score)
        send_evaluate_message(judge.tel_id, criterion_number, last_msg_id=judge.last_message, project_id=project_id)
        judge.criterion = db.fetch_criterions(judge)[criterion_number]
        judge.save()
    elif command == "c_fe":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        project = db.get_project_by_id(project_id)
        label = db.get_lang_obj(judge.lang.code)
        if db.all_marks_set(judge, project):
            bot.delete_message(judge.tel_id, judge.last_message)
            bot.send_message(judge.tel_id, text=label.msg_you_have_finished.format(project.title))
            db.confirm_finish(judge, project_id)
            notify_menu_service(call)
        else:
            bot.answer_callback_query(call.id, text=label.msg_you_need_to_finish, show_alert=True)
            first_criteria = db.fetch_first_unset_criteria(judge, project)
            judge.criterion = first_criteria
            judge.save()
            criterions = db.fetch_criterions(judge)
            send_evaluate_message(judge.tel_id, list(criterions).index(first_criteria), last_msg_id=judge.last_message)


def send_evaluate_message(judge_id, criterion_number=None, last_msg_id=None, project_id=None):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    criterions = db.fetch_criterions(judge)
    criterion = criterions[criterion_number]
    judge.criterion = criterion
    judge.save()
    current_mark = db.get_mark(judge, project_id, criterion_number)
    team_name, title, evaluate_team_markup, description, criterion_count, criterion_title = \
        mrk.evaluate_team_inline_keyboard(judge_id, criterion_number, read_only=criterion.read_only)
    msg_text = "*{}\n{}\n{} ({}/{})*\n\n{}\n\n*{}:* `{}`".format(
        team_name,
        criterion_title,
        title,
        criterion_number + 1,
        criterion_count,
        description if description is not None else label.msg_none,
        label.msg_current_mark,
        current_mark if current_mark is not None else label.msg_none
    )
    if last_msg_id is None:
        sent_message = bot.send_message(
            text=msg_text,
            chat_id=judge_id,
            parse_mode="markdown",
            reply_markup=evaluate_team_markup
        )
        judge.last_message = sent_message.message_id
        judge.save()
    else:
        bot.edit_message_text(
            text=msg_text,
            chat_id=judge_id,
            message_id=last_msg_id,
            reply_markup=evaluate_team_markup,
            parse_mode="markdown"
        )


def resend_evaluation(judge_id, by_last_visited_criterion=False, media=False):
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    if media:
        bot.send_message(judge_id, text=label.msg_your_media_was_saved, reply_markup=mrk.MAIN_MENU_MARKUP)
    else:
        bot.send_message(judge_id, text=label.msg_you_need_to_finish, reply_markup=mrk.MAIN_MENU_MARKUP)

    if by_last_visited_criterion:
        criterion = judge.criterion
    else:
        criterion = db.fetch_first_unset_criteria(judge, judge.project)
        judge.criterion = criterion

    criterions = db.fetch_criterions(judge)
    send_evaluate_message(judge_id, criterion_number=list(criterions).index(criterion))
    bot.delete_message(judge_id, judge.last_message)


def notify_menu_service(call):
    body = pickle.dumps(
        {
            "command": "send_team_select",
            "message": None,
            "call": call
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[EVALUATION] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

