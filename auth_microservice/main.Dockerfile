FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./auth_microservice/ /src/auth_microservice/
CMD [ "python", "./auth_microservice/bot.py" ]
