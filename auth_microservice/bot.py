#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import sys
import pickle
import logging

from telebot import TeleBot, apihelper, types, logger

from core import markup
from core import database as db
from core.data import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    bot.process_new_messages([body["message"]])


def extract_token(text):
    # Extracts token from the /start command.
    return text.split()[1] if len(text.split()) > 1 else None


@bot.message_handler(commands=["start"])
def start(message):
    """
        Checks tokens and judge states.

    `If two users have the same token the last who used it to log in would be considered as an active.`
    `Users can't log out once they log in. To do that use database._drop_token`
    """
    j_id = message.from_user.id
    judge = db.get_judge(j_id)
    if judge:
        send_previously_logged_in(j_id)
        db.set_state(judge.id, s.State.MENU.value)
        notify_menu_service(message)
    else:
        token = extract_token(message.text)
        if token and db.confirm_token(j_id, token):
            send_welcome(j_id)
        elif token:
            wrong_token(j_id)
        else:
            no_token(j_id)
    return True


@bot.message_handler(commands=["logout"])
def logout_handler(message):
    """
        Logout form current user
    """
    j_id = message.from_user.id
    db._disunite_by_tel_id(j_id)
    bot.send_message(chat_id=j_id,
                     text="Logged out")


@bot.message_handler(content_types=["text"])
def token_handler(message):
    """
        Checks token in case of another type of input
    """
    j_id = message.from_user.id
    if message.text and db.confirm_token(j_id, message.text):
        send_welcome(j_id)
        db.set_state(j_id, s.State.MENU.value)
        notify_menu_service(message)
    else:
        label = db.get_lang_obj(s.DEF_LANG)
        bot.send_message(chat_id=j_id,
                         parse_mode="markdown",
                         text=label.msg_wrong_token)


def send_previously_logged_in(judge_id):
    """
        Sends message to a user if he is already logged in
    """
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    bot.send_message(chat_id=judge.tel_id,
                     parse_mode="markdown",
                     text= label.msg_already_logged_in,
                     reply_markup=markup.MAIN_MENU_MARKUP)


def send_welcome(judge_id):
    """
        Sends message to a user if he is already logged in
    """
    judge = db.get_judge(judge_id)
    label = db.get_lang_obj(judge.lang.code)
    bot.send_message(chat_id=judge.tel_id,
                     parse_mode="markdown",
                     text=label.msg_welcome.format(
                         judge.full_name, judge.lang.code
                     ),
                     reply_markup=markup.MAIN_MENU_MARKUP)


def wrong_token(judge_id):
    """
        Token wasn't found or was taken
    """
    label = db.get_lang_obj(s.DEF_LANG)
    bot.send_message(chat_id=judge_id,
                     parse_mode="markdown",
                     text=label.msg_wrong_token)


def no_token(judge_id):
    """
        Token wasn't found at /start message
    """
    label = db.get_lang_obj(s.DEF_LANG)
    bot.send_message(chat_id=judge_id,
                     parse_mode="markdown",
                     text=label.msg_token_request)


def notify_menu_service(message):
    """
        Sends the notification to a menu service to send team_select
        See main_menu.send_team_select
    """
    body = {"command": "send_team_select",
            "message": message,
            "call": None}

    body = pickle.dumps(body)
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


if __name__ == '__main__':

    con = PickleConsumer(s.RMQ, 5672, 'auth', callback, exchange='rate_bot', logger='auth', username=s.RMQ_USER,
                         password=s.RMQ_PASS)
    try:
        logger.log(logging.DEBUG, '[AUTH] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
