#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, types, logger

from core.data import settings as s
from core import database as db
from core import markup as mrk
from core.consumer import PickleConsumer


if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'media', callback, exchange='rate_bot', logger='media', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge = db.get_judge(call.from_user.id)
    command = re.search('(?<=/)\w+', call.data).group(0)
    project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
    round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
    label = db.get_lang_obj(judge.lang.code)
    media = db.get_judge_media(call.from_user.id, project_id, round_id)
    if command == "c_gv":
        voices = media.filter(media_type='voice')
        if voices:
            for media_obj in voices:
                bot.send_voice(call.from_user.id, media_obj.media_id)
            if judge.state == s.State.ARCHIVE.value:
                notify_archive_service(call, project_id, round_id)
            else:
                notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, label.msg_no_voice)
    elif command == "c_gp":
        photos = media.filter(media_type='photo')
        if photos:
            for media_obj in photos:
                bot.send_photo(call.from_user.id, media_obj.media_id)
            if judge.state == s.State.ARCHIVE.value:
                notify_archive_service(call, project_id, round_id)
            else:
                notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, label.msg_no_photo)
    elif command == "c_gvi":
        video = media.filter(media_type='video')
        if video:
            for media_obj in video:
                bot.send_video(call.from_user.id, media_obj.media_id)
            if judge.state == s.State.ARCHIVE.value:
                notify_archive_service(call, project_id, round_id)
            else:
                notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, label.msg_no_video)


@bot.message_handler(content_types=["voice", "video", "photo"])
def handle_media(message):
    judge = db.get_judge(message.from_user.id)
    notify_eval_service(message)
    if message.content_type == "photo":
        file_id = message.photo[-1].file_id
        db.create_media(judge, file_id, f_type="photo")
    elif message.content_type == "voice":
        file_id = message.voice.file_id
        db.create_media(judge, file_id, f_type="voice")
    else:
        file_id = message.video.file_id
        db.create_media(judge, file_id, f_type="video")


def notify_eval_service(message):
    body = pickle.dumps({
            "command": "resend_evaluation",
            "message": message,
            "call": None
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="evaluation",
        body=body
    )


def notify_menu_service(call, project_id, round_id):
    body = pickle.dumps({
        "command": "send_project_card",
        "args": [project_id, round_id],
        "message": None,
        "call": call
    })
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


def notify_archive_service(call, project_id, round_id):
    body = pickle.dumps({
        "command": "send_project_card",
        "args": [project_id, round_id],
        "message": None,
        "call": call
    })
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="archive",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[MEDIA] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
