FROM mdmxfry/python

ADD  ./core/ /src/core/
ADD  ./media_microservice/ /src/media_microservice/
CMD [ "python", "./media_microservice/bot.py" ]
